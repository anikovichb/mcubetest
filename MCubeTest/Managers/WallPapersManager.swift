//
//  WallPapersManager.swift
//  MCube
//
//  Created by Vadim on 28.03.2019.
//  Copyright © 2019 Vadim. All rights reserved.
//

import UIKit

class WallPapersManager: NSObject {
    
    static let sharedInstance = WallPapersManager()
    
    var wallPapers: [WallPaper?]
    
    private override init() {
        self.wallPapers = []
    }
    
    
    func wallPapersFromDictionary(dict: [String: Any]) {
        self.wallPapers = []
        
        let responceWallPapersDictionaryArrayMeta = dict["meta"] as? [String: Any]
        let perPage = responceWallPapersDictionaryArrayMeta?["per_page"] as! Int
        var total = responceWallPapersDictionaryArrayMeta?["total"] as! Int
        
        while total > 0{
            total -= 1
            self.wallPapers.append(nil)
        }
        
        // Получаем массив словарей по ключу data
        let responceWallPapersDictionaryArray = dict["data"] as? [[String: Any]]
        var i = 0
        for wallPaperDictionary in responceWallPapersDictionaryArray!{
            let wallPaperId = wallPaperDictionary["id"] as! String
            let imagePath = wallPaperDictionary["image_path"] as! String
            let moviePath = wallPaperDictionary["movie_path"] as! String
            let createdAt = wallPaperDictionary["created_at"] as! String
            let updatedAt = wallPaperDictionary["updated_at"] as! String
            
            let wallPaper: WallPaper = WallPaper(wallPaperId: wallPaperId, imagePath: imagePath, moviePath: moviePath, createdAt: createdAt, updatedAt: updatedAt)
            self.wallPapers[i] = wallPaper
            
            i += 1
        }
    }
    
    
    func getWallpaperModel() -> [WallPaper?]{
        return wallPapers
    }
}
