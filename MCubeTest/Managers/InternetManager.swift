//
//  InternetManager.swift
//  MCube
//
//  Created by Vadim on 28.03.2019.
//  Copyright © 2019 Vadim. All rights reserved.
//

import UIKit
import AFNetworking


class InternetManager: NSObject {
    
    static let sharedInstance = InternetManager()
    
    private override init() {
        
    }
    
    func getJson(url: String, completion: @escaping (Bool, [String: Any]?) -> ()){
        let manager = AFHTTPSessionManager()
        manager.get(
            url,
            parameters: nil,
            success: {
                (operation, responseObject) in
                let dic = responseObject as? [String: Any]
                completion(true, dic)
        },
            failure: {
                (operation, error) in
                completion(false, nil)
                print("Error: " + error.localizedDescription)
        })
    }
}
