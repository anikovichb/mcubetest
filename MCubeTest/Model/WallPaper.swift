//
//  WallPaper.swift
//  MCube
//
//  Created by Vadim on 28.03.2019.
//  Copyright © 2019 Vadim. All rights reserved.
//

import UIKit

class WallPaper: NSObject {
    
    var wallPaperId: String!
    var imagePath: URL?
    var moviePath: URL?
    var createdAt: String!
    var updatedAt: String!
    
    init(wallPaperId: String, imagePath: String, moviePath: String, createdAt: String, updatedAt: String){
        self.wallPaperId = wallPaperId
        self.imagePath = URL(string: imagePath)
        self.moviePath = URL(string: moviePath)
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}
