//
//  PreviewCell.swift
//  MCube
//
//  Created by Vadim on 28.03.2019.
//  Copyright © 2019 Vadim. All rights reserved.
//

import UIKit

class PreviewCell: UICollectionViewCell {
    
    var previewImageView: UIImageView!

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black
        self.previewImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        self.addSubview(self.previewImageView)
        
    }
    
    func setParam(previewImage: UIImage){
        self.previewImageView.image = previewImage
    }
    
}
