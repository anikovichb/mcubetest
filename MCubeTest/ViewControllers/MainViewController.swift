//
//  MainViewController.swift
//  MCube
//
//  Created by Vadim on 27.03.2019.
//  Copyright © 2019 Vadim. All rights reserved.
//

import UIKit
import SDWebImage

class MainViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var previewCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.previewCollectionView.register(PreviewCell.self, forCellWithReuseIdentifier: "PreviewCellID")
        
        InternetManager.sharedInstance.getJson(url: "https://wallpapers.mediacube.games/api/photos", completion: {(success, responceDictionary) -> Void in
            if success {
                WallPapersManager.sharedInstance.wallPapersFromDictionary(dict: responceDictionary!)
                DispatchQueue.main.async {
                    self.previewCollectionView.reloadData()
                }
            }
        })
    }
    
    // MARK: UICollectionView delegate methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return WallPapersManager.sharedInstance.wallPapers.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let previewCell = previewCollectionView.dequeueReusableCell(withReuseIdentifier: "PreviewCellID", for: indexPath as IndexPath) as! PreviewCell
        let wallPaper = WallPapersManager.sharedInstance.getWallpaperModel()[indexPath.item]
        
        previewCell.previewImageView.image = nil
        
        if wallPaper != nil {
            if wallPaper!.imagePath != nil{
                if wallPaper!.imagePath?.absoluteString != ""{
                    previewCell.previewImageView.sd_setImage(with: wallPaper!.imagePath, placeholderImage: nil)
                }
            }
        }

//        Опционально - показ нумерации ячеек коллекшена
        
//        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 40, height: 30))
//        label.backgroundColor = UIColor.green
//        label.text = "\(indexPath.item)"
//        previewCell.addSubview(label)
        
        return previewCell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedCell = collectionView.cellForItem(at: indexPath) as! PreviewCell
        let livePhotoViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "livePhotoViewControllerID") as! LivePhotoViewController
        livePhotoViewController.currentWallpaper = WallPapersManager.sharedInstance.getWallpaperModel()[indexPath.item]
        livePhotoViewController.wallpaperPreviewImage = selectedCell.previewImageView.image
        self.navigationController?.pushViewController(livePhotoViewController, animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.previewCollectionView.frame.width / 3.0 - 11.0, height: self.previewCollectionView.frame.height / 5.0 - 20.0)
        

    }
}
